/**
  ******************************************************************************
  * File   : GPIO/LED_Toggle/main.c
  * Version: V1.2.9
  * Date   : 2021-01-15
  * Brief  : Main program body
  ******************************************************************************
  */

#include "bsp_led.h"
#include "bsp_uart.h"
#include "bsp_rtc.h"	

#include "at32_board.h"
#include "eeprom.h"

int main(void)
{

	Delay_init();
	BSP_USART1_Init(115200);
	printf("Hello,at32F407! Core clk = %d\r\n",SystemCoreClock);
	BSP_LEDs_Init();
	BSP_RTC_Init();
		
  for(;;)
  {
    BSP_LED_Toggle(LED2);
    Delay_ms(200);
    BSP_LED_Toggle(LED3);
    Delay_ms(200);
//	BSP_RTC_Time_Printf();
    BSP_LED_Toggle(LED4);
    Delay_ms(200);
  }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {}
}
#endif


