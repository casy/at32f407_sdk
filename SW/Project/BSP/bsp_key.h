#ifndef __BSP_KEY_H
#define __BSP_KEY_H

#include "stm32f10x.h"

/*
按键
*/
#define KEY1_PIN          GPIO_Pin_12	
#define KEY1_GPIO_Port    GPIOB
#define KEY1_GPIO_CLOK    RCC_APB2Periph_GPIOB

#define KEY_ON   1
#define KEY_OFF  0



/*
比较器1：避障左
比较器2：避障右
比较器3：循迹左
比较器4：循迹右
比较器5：循迹中间
*/
#define   COMPARE1_GPIO_CLK    RCC_APB2Periph_GPIOA
#define   COMPARE2_GPIO_CLK    RCC_APB2Periph_GPIOA
#define   COMPARE3_GPIO_CLK    RCC_APB2Periph_GPIOA
#define   COMPARE4_GPIO_CLK    RCC_APB2Periph_GPIOA
#define   COMPARE5_GPIO_CLK    RCC_APB2Periph_GPIOC

#define   COMPARE1_GPIO_Port   GPIOA
#define   COMPARE2_GPIO_Port   GPIOA
#define   COMPARE3_GPIO_Port   GPIOA
#define   COMPARE4_GPIO_Port   GPIOA
#define   COMPARE5_GPIO_Port   GPIOC

#define   COMPARE1_GPIO_Pin    GPIO_Pin_4
#define   COMPARE2_GPIO_Pin    GPIO_Pin_3
#define   COMPARE3_GPIO_Pin    GPIO_Pin_11
#define   COMPARE4_GPIO_Pin    GPIO_Pin_12
#define   COMPARE5_GPIO_Pin    GPIO_Pin_13


void Key_GPIO_Config(void);
uint8_t KEY_CHANGE(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

#endif

