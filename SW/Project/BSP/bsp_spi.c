#include "bsp_spi.h"

static __IO uint32_t  SPITimeout = SPIT_LONG_TIMEOUT; 

static void SPI_GPIO_Config(void)
{
	GPIO_InitType GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(	RCC_APB2PERIPH_GPIOA, ENABLE );
	GPIO_InitStructure.GPIO_Pins = SPI_SCK_PIN | SPI_MISO_PIN | SPI_MOSI_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;  //复用推挽输出 
	GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;
	GPIO_Init(SPI_SCK_GPIO_PORT, &GPIO_InitStructure);//初始化GPIO
	GPIO_SetBits( SPI_SCK_GPIO_PORT , SPI_SCK_PIN | SPI_MISO_PIN | SPI_MOSI_PIN); 
}

static void SPI_Mode_Config(void)
{
	SPI_InitType  SPI_InitStructure;

	RCC_APB2PeriphClockCmd(	SPI_CLK_Choice, ENABLE );
	SPI_InitStructure.SPI_TransMode = SPI_TRANSMODE_FULLDUPLEX;  //设置SPI单向或者双向的数据模式:SPI设置为双线双向全双工
	SPI_InitStructure.SPI_Mode = SPI_MODE_MASTER;		//设置SPI工作模式:设置为主SPI
	SPI_InitStructure.SPI_FrameSize = SPI_FRAMESIZE_8BIT;		//设置SPI的数据大小:SPI发送接收8位帧结构
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_HIGH;		//串行同步时钟的空闲状态为高电平
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2EDGE;	//串行同步时钟的第二个跳变沿（上升或下降）数据被采样
	SPI_InitStructure.SPI_NSSSEL = SPI_NSSSEL_SOFT;		//NSS信号由硬件（NSS管脚）还是软件（使用SSI位）管理:内部NSS信号有SSI位控制
	SPI_InitStructure.SPI_MCLKP = SPI_MCLKP_4;		//定义波特率预分频的值:波特率预分频值为256
	SPI_InitStructure.SPI_FirstBit = SPI_FIRSTBIT_MSB;	//指定数据传输从MSB位还是LSB位开始:数据传输从MSB位开始
	SPI_InitStructure.SPI_CPOLY = 7;				//CRC值计算的多项式
	
	SPI_Init(SPI_Choice, &SPI_InitStructure);  //根据SPI_InitStruct中指定的参数初始化外设SPIx寄存器
 
	SPI_Enable(SPI_Choice, ENABLE); //使能SPI外设
	
//	SPI_SendByte(0xff);//启动传输	
}

void BSP_SPI_Init(void)
{
	SPI_GPIO_Config();
	SPI_Mode_Config();
}

/**
  * @brief  等待超时回调函数
  * @param  None.
  * @retval None.
  */
static  uint16_t SPI_TIMEOUT_UserCallback(uint8_t errorCode)
{
	  /* 等待超时后的处理,输出错误信息 */
	  SPI_ERROR("SPI timeout !errorCode = %d",errorCode);
	  return 0;
}

 /**
  * @brief  使用SPI发送一个字节的数据
  * @param  byte：要发送的数据
  * @retval 返回接收到的数据
  */
u8 SPI_SendByte(u8 byte)
{
	SPITimeout = SPIT_FLAG_TIMEOUT;

	/* 等待发送缓冲区为空，TXE事件 */
	while (SPI_I2S_GetFlagStatus(SPI_Choice, SPI_I2S_FLAG_TE) == RESET)
	{
		if((SPITimeout--) == 0) return SPI_TIMEOUT_UserCallback(0);
	}

	/* 写入数据寄存器，把要写入的数据写入发送缓冲区 */
	SPI_I2S_TxData(SPI_Choice, byte);
	SPITimeout = SPIT_FLAG_TIMEOUT;

	/* 等待接收缓冲区非空，RXNE事件 */
	while (SPI_I2S_GetFlagStatus(SPI_Choice, SPI_I2S_FLAG_RNE) == RESET)
	{
		if((SPITimeout--) == 0) return SPI_TIMEOUT_UserCallback(1);
	}

	/* 读取数据寄存器，获取接收缓冲区数据 */
	return SPI_I2S_RxData(SPI_Choice);
}

 /**
  * @brief  使用SPI读取一个字节的数据
  * @param  无
  * @retval 返回接收到的数据
  */
u8 SPI_ReadByte(void)
{
	return (SPI_SendByte(Dummy_Byte));
}


