/**
  **************************************************************************
  * File   : at32_board.c
  * Version: V1.2.9
  * Date   : 2021-01-15
  * Brief  : 1. Set of firmware functions to manage Leds, push-button and COM ports.
  *          2. initialize Delay Function and USB
  **************************************************************************
  */

#include "bsp_uart.h"

static void USART1_Config(uint32_t bound);
static void USART1_NVIC(void);
static void USART1_DMA_Config(void);


#define LENGTH	100

unsigned int get_len = 0;
unsigned char RxBuffer[LENGTH];
unsigned char Buffer[LENGTH];

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
int fputc(int ch, FILE *f) 
{  
	 USART_SendData(AT32_PRINT_UART, ch);
	 while ( USART_GetFlagStatus(AT32_PRINT_UART, USART_FLAG_TRAC) == RESET );    
	 return ch;
}


/**
  * @brief  initialize UART1 for debug  
  * @param  bound: UART BaudRate
  * @retval None
  */
static void USART1_Config(uint32_t bound)
{
	GPIO_InitType GPIO_InitStructure;
	USART_InitType USART_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_GPIOA, ENABLE);	
	RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_USART1, ENABLE);

	/* Configure the UART1 TX pin */
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pins = AT32_PRINT_UARTTX_PIN; 
	GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(AT32_PRINT_UARTTX_GPIO, &GPIO_InitStructure);

	/* Configure the UART1 RX pin */
	GPIO_InitStructure.GPIO_Pins = AT32_PRINT_UARTRX_PIN;//PA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_PD;
	GPIO_Init(AT32_PRINT_UARTRX_GPIO, &GPIO_InitStructure);

	/*Configure UART param*/
	USART_StructInit(&USART_InitStructure);
	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	

	USART_Init(USART1, &USART_InitStructure); 
	//USART_INTConfig(USART1, USART_INT_RDNE, ENABLE);
	USART_INTConfig(USART1, USART_INT_IDLEF, ENABLE);
	USART_Cmd(USART1, ENABLE); 	
}


static void USART1_NVIC(void)
{
  NVIC_InitType NVIC_InitStructure;
	
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);	
}

static void USART1_DMA_Config(void)
{
  DMA_InitType DMA_InitStructure;
  RCC_AHBPeriphClockCmd(RCC_AHBPERIPH_DMA1, ENABLE);
  DMA_Reset(DMA1_Channel5);
  DMA_DefaultInitParaConfig(&DMA_InitStructure);
  DMA_InitStructure.DMA_PeripheralBaseAddr = 0x40013804;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)RxBuffer;
  DMA_InitStructure.DMA_Direction = DMA_DIR_PERIPHERALSRC;
  DMA_InitStructure.DMA_BufferSize = LENGTH;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PERIPHERALINC_DISABLE;
  DMA_InitStructure.DMA_MemoryInc = DMA_MEMORYINC_ENABLE;
  DMA_InitStructure.DMA_PeripheralDataWidth = DMA_PERIPHERALDATAWIDTH_BYTE;
  DMA_InitStructure.DMA_MemoryDataWidth = DMA_MEMORYDATAWIDTH_BYTE;
  DMA_InitStructure.DMA_Mode = DMA_MODE_CIRCULAR;
  DMA_InitStructure.DMA_Priority = DMA_PRIORITY_VERYHIGH;
  DMA_InitStructure.DMA_MTOM = DMA_MEMTOMEM_DISABLE;
  DMA_Init(DMA1_Channel5, &DMA_InitStructure);
	
  USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE);
  DMA_ChannelEnable(DMA1_Channel5, ENABLE); 
}

void BSP_USART1_Init(uint32_t baud)
{	
	USART1_Config(baud);
	USART1_NVIC();
	USART1_DMA_Config(); 
}


void USART1_IRQHandler(void)
{
    if(USART_GetITStatus(USART1, USART_INT_IDLEF) != RESET)
    {
		DMA_ChannelEnable(DMA1_Channel5, DISABLE); 						//关闭DMA											
		get_len = LENGTH - DMA_GetCurrDataCounter(DMA1_Channel5);		//获取实际接收数据长度
#if 0		
		for (int i = 0; i < get_len;i++)
		{
			USART_SendData(AT32_PRINT_UART, RxBuffer[i]);
			while ( USART_GetFlagStatus(AT32_PRINT_UART, USART_FLAG_TRAC) == RESET );  
		}
#endif	
		DMA_SetCurrDataCounter(DMA1_Channel5, LENGTH);  // Reset DMA data

		DMA_ChannelEnable(DMA1_Channel5, ENABLE); 	//开启DMA
		USART_ReceiveData(USART1);					//清除标志位,Must add here.s
    }
}


