#ifndef __BSP_I2C_H
#define __BSP_I2C_H

#include "at32f4xx.h"
#include "AT32f4xx_i2c.h"
#include "./UART/bsp_uart.h"
#include "SysTick.h"

#define I2C1_PinsRemap  		1	//I2C1端口是否重映射		0,不使用.1,使用.

#define I2C_Speed            4000
#define I2C_OWN_ADDRESS7     0X0A

#define I2C_Choice           I2C1
#define I2C_CLK              RCC_APB1PERIPH_I2C1
//#define I2C_ADDRESS          0x78

#if I2C1_PinsRemap
#define I2C_SCL_PIN            GPIO_Pins_8
#define I2C_SCL_GPIO_PORT      GPIOB
#define I2C_SCL_GPIO_CLK       RCC_APB2PERIPH_GPIOB

#define I2C_SDA_PIN            GPIO_Pins_9
#define I2C_SDA_GPIO_PORT      GPIOB
#define I2C_SDA_GPIO_CLK       RCC_APB2PERIPH_GPIOB

#else
#define I2C_SCL_PIN            GPIO_Pins_6
#define I2C_SCL_GPIO_PORT      GPIOB
#define I2C_SCL_GPIO_CLK       RCC_APB2PERIPH_GPIOB

#define I2C_SDA_PIN            GPIO_Pins_7
#define I2C_SDA_GPIO_PORT      GPIOB
#define I2C_SDA_GPIO_CLK       RCC_APB2PERIPH_GPIOB

#endif

#define I2CT_FLAG_TIMEOUT     ((uint16_t)0x1000)
#define I2CT_LONG_TIMEOUT     ((uint16_t)(10 * I2CT_FLAG_TIMEOUT))


/*信息输出*/
#define EEPROM_DEBUG_ON         0

#define I2C_INFO(fmt,arg...)           printf("<<-EEPROM-INFO->> "fmt"\n",##arg)     //##args表示参数个数可变
#define I2C_ERROR(fmt,arg...)          printf("<<-EEPROM-ERROR->> "fmt"\n",##arg)
#define I2C_DEBUG(fmt,arg...)          do{\
                                          if(EEPROM_DEBUG_ON)\
                                          printf("<<-EEPROM-DEBUG->> [%d]"fmt"\n",__LINE__, ##arg);\
                                          }while(0)



void I2C_PERIPH_Init(void);																					
void I2C_ByteWrite(u8 Device_Addr ,u8 WriteAddr,u8 pBuffer);																			
u8 I2C_ByteRead(u8 Device_Addr ,u8 WriteAddr);																					
u8 I2C_Bytes_Write(u8 Device_Addr ,u8 addr,u8 *pBuffer,  u16 Nume);																					
u8 I2C_page_Write(u8 Device_Addr ,u8 *pBuffer, u8 addr, u16 Nume);																					
u8 I2C_BufferRead(u8 Device_Addr ,u8 ReadAddr, u16 NumByteToRead,u8* read);																				

																					
#endif
