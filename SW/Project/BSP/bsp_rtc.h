/**
  ******************************************************************************
  * File   : RTC/Calendar/rtc.h 
  * Version: V1.2.9
  * Date   : 2021-01-15
  * Brief  : This file contains the headers of the calendar API.
  ******************************************************************************
  */ 
 

#ifndef __BSP_RTC_H
#define __BSP_RTC_H	    

#include "at32f4xx.h"
#include <stdio.h>

typedef struct 
{
	__IO uint8_t hour;
	__IO uint8_t min;
	__IO uint8_t sec;			
	__IO uint16_t w_year;
	__IO uint8_t  w_month;
	__IO uint8_t  w_date;
	__IO uint8_t  week;		 
}calendar_obj;


extern calendar_obj calendar;	
extern const uint8_t mon_table[12];
extern const uint8_t table_week[12];
extern char const weekday_table[7][10];

void BSP_RTC_Init(void);   
void BSP_RTC_Get(void);
uint8_t BSP_RTC_Set(uint16_t syear, uint8_t smon, uint8_t sday, uint8_t hour, uint8_t min, uint8_t sec);
uint8_t BSP_RTC_Alarm_Set(uint16_t syear, uint8_t smon, uint8_t sday, uint8_t hour, uint8_t min, uint8_t sec);
uint8_t BSP_RTC_Get_Week(uint16_t year, uint8_t month, uint8_t day);
void BSP_RTC_Time_Printf(void);

uint8_t Is_Leap_Year(uint16_t year);

#endif

