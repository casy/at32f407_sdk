#ifndef __BSP_SPI_FLASH_H
#define __BSP_SPI_FLASH_H			    

#include "bsp_spi.h"

typedef u8 uint8_t;
typedef u32 uint32_t;

//CS(NSS)
#define Flash_CS_PIN               GPIO_Pins_15
#define Flash_CS_GPIO_PORT         GPIOA
#define Flash_CS_GPIO_CLK          RCC_APB2PERIPH_GPIOA

#define Flash_CS_LOW()             {Flash_CS_GPIO_PORT->BRE=Flash_CS_PIN;}
#define Flash_CS_HIGH()            {Flash_CS_GPIO_PORT->BSRE=Flash_CS_PIN;}

extern u16 SPI_FLASH_TYPE;		//定义flash芯片型号		
			 
extern u8 SPI_FLASH_BUF[4096];

//Flash读写
#define FLASH_ID                  0x1c17
//指令表
#define Flash_WriteEnable		  0x06 
#define Flash_WriteDisable		  0x04 
#define Flash_ReadStatusReg		  0x05 
#define Flash_WriteStatusReg		0x01 
#define Flash_ReadData			    0x03 
#define Flash_FastReadData		  	0x0B 
#define Flash_FastReadDual		  	0x3B 
#define Flash_PageProgram		    0x02 
#define Flash_BlockErase			0xD8 
#define Flash_SectorErase		    0x20 
#define Flash_ChipErase			    0xC7 
#define Flash_PowerDown		  	  	0xB9 
#define Flash_ReleasePowerDown		0xAB 
#define Flash_DeviceID			    0xAB 
#define Flash_ManufactDeviceID		0x90 
#define Flash_JedecDeviceID		  	0x9F 

void SPI_Flash_Init(void);
u16  SPI_Flash_ReadID(void);  	    //读取FLASH ID
u8	 SPI_Flash_ReadSR(void);        //读取状态寄存器 
void SPI_FLASH_Write_SR(u8 sr);  	//写状态寄存器
void SPI_FLASH_Write_Enable(void);  //写使能 
void SPI_FLASH_Write_Disable(void);	//写保护
void SPI_Flash_Read(u8* pBuffer,u32 ReadAddr,u16 NumByteToRead);   //读取flash
void SPI_Flash_Write(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite);//写入flash
void SPI_Flash_Write_Page(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite);
void SPI_Flash_Write_NoCheck(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite);
void SPI_Flash_Erase_Chip(void);    	  //整片擦除
void SPI_Flash_Erase_Sector(u32 Dst_Addr);//扇区擦除
void SPI_Flash_Erase_Block(u32 Dst_Addr);
void SPI_Flash_Wait_Busy(void);           //等待空闲
void SPI_Flash_PowerDown(void);           //进入掉电模式
void SPI_Flash_WAKEUP(void);			  //唤醒
#endif



