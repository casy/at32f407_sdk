#include "bsp_key.h"

void Key_GPIO_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(KEY1_GPIO_CLOK,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin   =  KEY1_PIN;
	GPIO_InitStructure.GPIO_Mode  =  GPIO_Mode_IPU;
	GPIO_Init(KEY1_GPIO_Port,&GPIO_InitStructure);
}

uint8_t KEY_CHANGE(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
   if((GPIO_ReadInputDataBit(GPIOx,GPIO_Pin))==KEY_ON)
	 {
	   while((GPIO_ReadInputDataBit(GPIOx,GPIO_Pin))==KEY_ON);
			 return KEY_ON;
	 }
	 else 
	 return KEY_OFF; 
 }

void Compare_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(COMPARE1_GPIO_CLK|COMPARE5_GPIO_CLK,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin   =  COMPARE1_GPIO_Pin|COMPARE2_GPIO_Pin|COMPARE3_GPIO_Pin|COMPARE4_GPIO_Pin;
	GPIO_InitStructure.GPIO_Mode  =  GPIO_Mode_IPU;
	
	GPIO_Init(COMPARE1_GPIO_Port, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin   =  COMPARE5_GPIO_Pin;
	GPIO_Init(COMPARE5_GPIO_Port, &GPIO_InitStructure);
}	
