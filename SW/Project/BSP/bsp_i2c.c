#include "bsp_i2c.h"
#include "./UART/bsp_uart.h"
#include <stdio.h>


static u16 I2C_Timeout;

static void I2c_GPIO_Config(void)	
{
  GPIO_InitType GPIO_InitStructure;

#if I2C1_PinsRemap	
  RCC_APB2PeriphClockCmd( I2C_SCL_GPIO_CLK|RCC_APB2PERIPH_AFIO, ENABLE);     // Ê¹ÄÜPC¶Ë¿ÚÊ±ÖÓ 	
  GPIO_PinsRemapConfig(GPIO_Remap_I2C1,ENABLE);
#else
	RCC_APB2PeriphClockCmd(I2C_SCL_GPIO_CLK, ENABLE); 
#endif	
  GPIO_InitStructure.GPIO_Pins = I2C_SCL_PIN|I2C_SDA_PIN;	
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;       
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;
	
  GPIO_Init(I2C_SCL_GPIO_PORT, &GPIO_InitStructure);     //³õÊ¼»¯PC¶Ë¿Ú
}

static void I2C_Mode_Config(void)
{
	I2C_InitType     I2C_InitStructure;
	
	RCC_APB1PeriphClockCmd( I2C_CLK, ENABLE); 
	
	I2C_InitStructure.I2C_Mode        = I2C_Mode_I2CDevice;
	I2C_InitStructure.I2C_FmDutyCycle   = I2C_FmDutyCycle_16_9;
	I2C_InitStructure.I2C_OwnAddr1 = I2C_OWN_ADDRESS7;
	I2C_InitStructure.I2C_Ack         = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AddrMode  =  I2C_AddrMode_7bit;
	I2C_InitStructure.I2C_BitRate  = I2C_Speed;
	
	I2C_Init(I2C_Choice,&I2C_InitStructure);
	I2C_Cmd(I2C_Choice,ENABLE);
}

void I2C_PERIPH_Init(void)
{
	I2c_GPIO_Config();
	I2C_Mode_Config();
}

static u32 I2C_TIMEOUT_UserCallback(u8 errorCode)
{
	I2C_ERROR("\n\r I2C!errorCode = 0X%x\n\r",errorCode);
	return 0;
}

void I2C_WaitEepromStandbyState(u8 Device_Addr)      
{
  vu16 i,SR1_Tmp = 0;

  do
  {
    /* Send START condition */
    I2C_GenerateSTART(I2C_Choice, ENABLE);
    /* Read I2C_Choice SR1 register */
    SR1_Tmp = I2C_ReadRegister(I2C_Choice, I2C_Register_STS1);
    /* Send EEPROM address for write */
    I2C_Send7bitAddress(I2C_Choice, Device_Addr, I2C_Direction_Transmit);
  }while(!(I2C_ReadRegister(I2C_Choice, I2C_Register_STS1) & 0x0002));
  
  /* Clear AF flag */
  I2C_ClearFlag(I2C_Choice, I2C_FLAG_ACKFAIL);
  /* STOP condition */    
  I2C_GenerateSTOP(I2C_Choice, ENABLE);\
	
	i=I2CT_FLAG_TIMEOUT;
  while(i--);

	//printf("\n\r¼ì²âÍê±Ï");
}

void I2C_ByteWrite(u8 Device_Addr ,u8 WriteAddr,u8 pBuffer)
{
	u16 I2C_Timeout;
	
	while(I2C_GetFlagStatus(I2C_Choice, I2C_FLAG_BUSYF));
	
	/*ÆðÊ¼ÐÅºÅ*/
	I2C_GenerateSTART(I2C_Choice, ENABLE);
	I2C_Timeout = I2CT_FLAG_TIMEOUT;
	/* ?? EV5 ???????*/
	while (!I2C_CheckEvent(I2C_Choice, I2C_EVENT_MASTER_START_GENERATED))
	{
	if ((I2C_Timeout--) == 0)  
		I2C_TIMEOUT_UserCallback(0);
	}

	/*Éè±¸µØÖ·*/
	I2C_Send7bitAddress(I2C_Choice, Device_Addr,I2C_Direction_Transmit);
	I2C_Timeout = I2CT_FLAG_TIMEOUT;
	/* ?? EV6 ???????*/
	while (!I2C_CheckEvent(I2C_Choice,I2C_EVENT_MASTER_ADDRESS))
	{
	if ((I2C_Timeout--) == 0) 
		I2C_TIMEOUT_UserCallback(1);
	}

	/* Ð´µØÖ·*/
	I2C_SendData(I2C_Choice, WriteAddr);
	I2C_Timeout = I2CT_FLAG_TIMEOUT;
	/* ?? EV8 ???????*/
	while (!I2C_CheckEvent(I2C_Choice,I2C_EVENT_MASTER_DATA_TRANSMITTING))
	{
	if ((I2C_Timeout--) == 0) 
		I2C_TIMEOUT_UserCallback(2);
	}
	
	/* Ð´Êý¾Ý */
	I2C_SendData(I2C_Choice, pBuffer);
	I2C_Timeout = I2CT_FLAG_TIMEOUT;
	/* ?? EV8 ???????*/
	while (!I2C_CheckEvent(I2C_Choice,I2C_EVENT_MASTER_DATA_TRANSMITTED))
	{
	 if((I2C_Timeout--) == 0) 
		 I2C_TIMEOUT_UserCallback(3);
	}

	/* Í£Ö¹ÐÅºÅ*/
	I2C_GenerateSTOP(I2C_Choice, ENABLE);
}


/******************读的最后一B必须先不应达且停止位*****************************/
u8 I2C_ByteRead(u8 Device_Addr ,u8 WriteAddr)
{
	u8 Date;
	
	while(I2C_GetFlagStatus(I2C_Choice, I2C_FLAG_BUSYF));
	
	/*ÆðÊ¼ÐÅºÅ*/
	I2C_GenerateSTART(I2C_Choice, ENABLE);
	I2C_Timeout = I2CT_FLAG_TIMEOUT;
	/* ?? EV5 ???????*/
	while (!I2C_CheckEvent(I2C_Choice, I2C_EVENT_MASTER_START_GENERATED))
	{
	if ((I2C_Timeout--) == 0)  
		I2C_TIMEOUT_UserCallback(4);
	}

	/*Éè±¸µØÖ·*/
	I2C_Send7bitAddress(I2C_Choice, Device_Addr,I2C_Direction_Transmit);
	I2C_Timeout = I2CT_FLAG_TIMEOUT;
	/* ?? EV6 ???????*/
	while (!I2C_CheckEvent( I2C_Choice , I2C_EVENT_MASTER_ADDRESS))
	{
	if ((I2C_Timeout--) == 0) 
		I2C_TIMEOUT_UserCallback(5);
	}
	
	//	I2C_Cmd(I2C_Choice, ENABLE);

	/* Ð´µØÖ·*/
	I2C_SendData( I2C_Choice , WriteAddr);
	I2C_Timeout = I2CT_FLAG_TIMEOUT;
	
	/* ?? EV8 ???????*/
	while (!I2C_CheckEvent( I2C_Choice , I2C_EVENT_MASTER_DATA_TRANSMITTED))
	{
	if ((I2C_Timeout--) == 0) 
		I2C_TIMEOUT_UserCallback(6);
	}
	

	
	/************起始信号*************/
	I2C_GenerateSTART( I2C_Choice , ENABLE);
	I2C_Timeout = I2CT_FLAG_TIMEOUT;
	
	/* ?? EV5 ???????*/
	while (!I2C_CheckEvent( I2C_Choice , I2C_EVENT_MASTER_START_GENERATED))
	{
	if ((I2C_Timeout--) == 0)  
		I2C_TIMEOUT_UserCallback(7);
	}
	
	I2C_Send7bitAddress( I2C_Choice , Device_Addr , I2C_Direction_Receive);
	I2C_Timeout = I2CT_FLAG_TIMEOUT;
	
	/* ?? EV6 ???????*/
	while (!I2C_CheckEvent( I2C_Choice , I2C_EVENT_MASTER_ADDRESS_WITH_RECEIVER))
	{
	if ((I2C_Timeout--) == 0) 
		I2C_TIMEOUT_UserCallback(8);
	}
	
	  I2C_AcknowledgeConfig(I2C_Choice, DISABLE);  
	
		I2C_GenerateSTOP(I2C_Choice, ENABLE);
	
		I2C_Timeout = I2CT_FLAG_TIMEOUT;
	/* ?? EV7 ???????*/
	while (!I2C_CheckEvent(I2C_Choice, I2C_EVENT_MASTER_DATA_RECEIVED))
	{
	if ((I2C_Timeout--) == 0)  
		I2C_TIMEOUT_UserCallback(9);
  }
	/* Í£Ö¹ÐÅºÅ*/
	Date=I2C_ReceiveData(I2C_Choice);
	
	I2C_AcknowledgeConfig(I2C_Choice, ENABLE); 
	
	return Date;
}



/*   写多个字节   */
u8 I2C_Bytes_Write(u8 Device_Addr ,u8 addr,u8 *pBuffer,  u16 Nume)
{
	u16 i;
  
	for(i=0;i<Nume;i++)
	{
		I2C_WaitEepromStandbyState(Device_Addr);
		I2C_ByteWrite(Device_Addr, addr++,*(pBuffer++));
	}
	return 0;
}


u8 I2C_page_Write(u8 Device_Addr ,u8 *pBuffer, u8 addr, u16 Nume)
{
	int i;
	
	i=I2CT_FLAG_TIMEOUT;
	while(I2C_GetFlagStatus(I2C_Choice, I2C_FLAG_BUSYF))
	{
		if(i--)
			return I2C_TIMEOUT_UserCallback(11);	
	}
	
	I2C_WaitEepromStandbyState(Device_Addr);
	
	/*        ÆðÊ¼ÐÅºÅ   
	¼ì²âÊÇ·ñ³É¹¦£¬³¬¹ýÑÓÊ±ÍË³ö   */	
	I2C_GenerateSTART(I2C_Choice, ENABLE);
	while (!I2C_CheckEvent(I2C_Choice, I2C_EVENT_MASTER_START_GENERATED))
  {
		if ((i--)==0)
			return I2C_TIMEOUT_UserCallback(0);
  }
	
	/*   ·¢ËÍÆßÎ»´Ó»úµØÖ·£¨×óÒÆÒ»Î»£©
	         Ð´Êý¾Ý    */
	I2C_Send7bitAddress(I2C_Choice, Device_Addr,I2C_Direction_Transmit);
  i = I2CT_FLAG_TIMEOUT;
	while (!I2C_CheckEvent(I2C_Choice,I2C_EVENT_MASTER_ADDRESS))
  {
		if ((i--)==0)
			return I2C_TIMEOUT_UserCallback(1);
  }
	
	/*   ·¢ËÍ´Ó»úÄÚ²¿µØÖ·   */
	I2C_SendData(I2C_Choice,addr);
	i = I2CT_FLAG_TIMEOUT;
	while (!I2C_CheckEvent(I2C_Choice,I2C_EVENT_MASTER_DATA_TRANSMITTING))
  {
		if ((i--)==0)
			return I2C_TIMEOUT_UserCallback(2);
  }
	
	/*   ·¢ËÍNume¸öÊý¾Ý   */
	while(Nume--)
	{
		I2C_SendData(I2C_Choice,*pBuffer);
	  i = I2CT_FLAG_TIMEOUT;
	  while (!I2C_CheckEvent(I2C_Choice,I2C_EVENT_MASTER_DATA_TRANSMITTING))
     {
		   if ((i--)==0)
			 return I2C_TIMEOUT_UserCallback(2);
     }
		 pBuffer++;
	}
	
	I2C_GenerateSTOP(I2C_Choice,ENABLE );   //Í£Ö¹ÐÅºÅ
	
	return 1;	
}


/**
   读多个字节
  */
u8 I2C_BufferRead(u8 Device_Addr ,u8 ReadAddr, u16 NumByteToRead,u8* read)
{  
//	  int i;
    I2C_Timeout = I2CT_FLAG_TIMEOUT;
  //*((u8 *)0x4001080c) |=0x80; 
    while(I2C_GetFlagStatus(I2C_Choice, I2C_FLAG_BUSYF))   
    {
  //  if((I2C_Timeout--) == 0) return I2C_TIMEOUT_UserCallback(9);
    }
		
//		I2C_WaitEepromStandbyState(Device_Addr);
		
  /* Send START condition */
  I2C_GenerateSTART(I2C_Choice, ENABLE);
  //*((u8 *)0x4001080c) &=~0x80;
  
  I2C_Timeout = I2CT_FLAG_TIMEOUT;

  /* Test on EV5 and clear it */
  while(!I2C_CheckEvent(I2C_Choice, I2C_EVENT_MASTER_START_GENERATED))
  {
    if((I2C_Timeout--) == 0) return I2C_TIMEOUT_UserCallback(10);
   }

  /* Send EEPROM address for write */
  I2C_Send7bitAddress(I2C_Choice, Device_Addr, I2C_Direction_Transmit);

  I2C_Timeout = I2CT_FLAG_TIMEOUT;
 
  /* Test on EV6 and clear it */
  while(!I2C_CheckEvent(I2C_Choice, I2C_Direction_Transmit)) 
    {
    if((I2C_Timeout--) == 0) return I2C_TIMEOUT_UserCallback(11);
   }
  /* Clear EV6 by setting again the PE bit */
  I2C_Cmd(I2C_Choice, ENABLE);

  /* Send the EEPROM's internal address to write to */
  I2C_SendData(I2C_Choice, ReadAddr);  

     I2C_Timeout = I2CT_FLAG_TIMEOUT;
	 
	 
  /* Test on EV8 and clear it */
  while(!I2C_CheckEvent(I2C_Choice, I2C_EVENT_MASTER_DATA_TRANSMITTED))
    {
    if((I2C_Timeout--) == 0) return I2C_TIMEOUT_UserCallback(12);
   }
  /* Send STRAT condition a second time */  
  I2C_GenerateSTART(I2C_Choice, ENABLE);
  
     I2C_Timeout = I2CT_FLAG_TIMEOUT;

  /* Test on EV5 and clear it */
  while(!I2C_CheckEvent(I2C_Choice, I2C_EVENT_MASTER_START_GENERATED))
    {
    if((I2C_Timeout--) == 0) return I2C_TIMEOUT_UserCallback(13);
   }
  /* Send EEPROM address for read */
  I2C_Send7bitAddress(I2C_Choice, Device_Addr, I2C_Direction_Receive);
  
     I2C_Timeout = I2CT_FLAG_TIMEOUT;

  /* Test on EV6 and clear it */
  while(!I2C_CheckEvent(I2C_Choice, I2C_EVENT_MASTER_ADDRESS_WITH_RECEIVER))
    {
    if((I2C_Timeout--) == 0) return I2C_TIMEOUT_UserCallback(14);
   }
  /* While there is data to be read */
  while(NumByteToRead)  
  {
    if(NumByteToRead == 1)
    {
      /* Disable Acknowledgement */
      I2C_AcknowledgeConfig(I2C_Choice, DISABLE);
      
      /* Send STOP Condition */
      I2C_GenerateSTOP(I2C_Choice, ENABLE);
    }

		
		I2C_Timeout = I2CT_FLAG_TIMEOUT*10;
		while(I2C_CheckEvent(I2C_Choice, I2C_EVENT_MASTER_DATA_RECEIVED)==0)  
		{
			if((I2C_Timeout--) == 0) return I2C_TIMEOUT_UserCallback(3);
		} 	
		{
		  /* Read a byte from the device */
      *read = I2C_ReceiveData(I2C_Choice);

      /* Point to the next location where the byte read will be saved */
      read++; 
      
      /* Decrement the read bytes counter */
      NumByteToRead--;
		}			
  }

  /* Enable Acknowledgement to be ready for another reception */
  I2C_AcknowledgeConfig(I2C_Choice, ENABLE);
  
  return 1;
}



