#ifndef __BSP_ADC_H
#define __BSP_ADC_H 

#include "at32f4xx.h"
#include "at32f4xx_adc.h"

#define Adc1_Clk          RCC_APB2PERIPH_ADC1
#define Adc2_Clk          RCC_APB2PERIPH_ADC2

#define Adc1_GPIO_Clk     RCC_APB2PERIPH_GPIOA
#define Adc1_GPIO_Port    GPIOA
#define Adc1_GPIO_Pin     GPIO_Pins_4

#define Adc2_GPIO_Clk     RCC_APB2PERIPH_GPIOA
#define Adc2_GPIO_Port    GPIOA
#define Adc2_GPIO_Pin     GPIO_Pins_5

void  Adc_Init(void);
u16 Get_Adc(ADC_Type* ADCx, u8 ch);
u16 Get_Adc_Average(ADC_Type* ADCx,u8 ch,u8 times);

#endif

/*
             adc1             2              
通道0         A0              一样
通道1          1               
通道2          2
通道3          3
通道4          4
通道5          5
通道6          6
通道7          7
通道8         B0
通道9          1
通道10        C0
通道11         1
通道12         2
通道13         3
通道14         4
通道15         5
*/