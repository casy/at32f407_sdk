#ifndef __BSP_DHT12_H
#define __BSP_DHT12_H

#include "at32f4xx.h"


#define DHT12_I2C_ADDRESS            0xB8

void Dht12_Read(void);

#endif
