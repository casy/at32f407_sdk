#ifndef __BSP_EXTI_H
#define __BSP_EXTI_H

#include "at32f4xx.h"
#include "at32f4xx_exti.h"

#define NOA  		1	//使用A以外的端口外部中断需要打开复用

#define EXTI_PIN_Choice     GPIO_Pins_7
#define EXTI_GPIO_Choice    GPIOC
#define EXTI_CLOK_Choice    RCC_APB2PERIPH_GPIOC

#define EXTI_PortSource     GPIO_PortSourceGPIOC
#define EXTI_PinSource      GPIO_PinsSource7

#define EXTI_Line_Choice               EXTI_Line7

#define EXTI_IRQn_Choice               EXTI9_5_IRQn
#define EXTI_IRQHandler_Choice         EXTI9_5_IRQHandler


void EXTI_Config(void);

#endif

