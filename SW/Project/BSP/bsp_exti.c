#include "./EXIT/bsp_exti.h"


static void EXTI_GPIO_Init(void)
{
  GPIO_InitType GPIO_InitStructure;
	
#if NOA
  RCC_APB2PeriphClockCmd(EXTI_CLOK_Choice|RCC_APB2PERIPH_AFIO,ENABLE);
#else
  RCC_APB2PeriphClockCmd(EXTI_CLOK_Choice,ENABLE);
#endif	

  GPIO_InitStructure.GPIO_Pins  = EXTI_PIN_Choice;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_PD;

  GPIO_Init(EXTI_GPIO_Choice, &GPIO_InitStructure);
	
	GPIO_EXTILineConfig(EXTI_PortSource,EXTI_PinSource);  //将引脚连接到中断线上
}

static void EXTI_Mode_Init(void)
{
	EXTI_InitType EXTI_InitStructure;
	EXTI_ClearIntPendingBit(EXTI_Line_Choice);
	EXTI_InitStructure.EXTI_Line    = EXTI_Line_Choice;
  EXTI_InitStructure.EXTI_Mode    = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineEnable = ENABLE;
	
  EXTI_Init(&EXTI_InitStructure);
}

static void NVIC_Mode_Init(void)
{
  NVIC_InitType  NVIC_InitStructure;
	
//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
  NVIC_InitStructure.NVIC_IRQChannel = EXTI_IRQn_Choice;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
 
  NVIC_Init(&NVIC_InitStructure);
}	

void EXTI_Config(void)
{
	NVIC_Mode_Init();
	EXTI_GPIO_Init();
	EXTI_Mode_Init();

}

