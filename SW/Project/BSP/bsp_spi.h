#ifndef __BSP_SPI_H
#define __BSP_SPI_H


#include "at32f4xx_spi.h"
#include <stdio.h>


#define Dummy_Byte 			0xFF

/*等待超时时间*/
#define SPIT_FLAG_TIMEOUT         ((uint32_t)0x1000)
#define SPIT_LONG_TIMEOUT         ((uint32_t)(10 * SPIT_FLAG_TIMEOUT))

/*信息输出*/
#define SPI_DEBUG_ON         1

#define SPI_INFO(fmt,arg...)           printf("<<-SPI-INFO->> "fmt"\n",##arg)
#define SPI_ERROR(fmt,arg...)          printf("<<-SPI-ERROR->> "fmt"\n",##arg)
#define SPI_DEBUG(fmt,arg...)          do{\
                                          if(SPI_DEBUG_ON)\
                                          printf("<<-SPI-DEBUG->> [%d]"fmt"\n",__LINE__, ##arg);\
                                          }while(0)

//SPI X
#define SPI_Choice               SPI1
#define SPI_CLK_Choice           RCC_APB2PERIPH_SPI1

//SCK 
#define SPI_SCK_PIN              GPIO_Pins_5
#define SPI_SCK_GPIO_PORT        GPIOA
#define SPI_SCK_GPIO_CLK         RCC_APB2PERIPH_GPIOA


//MISO 
#define SPI_MISO_PIN             GPIO_Pins_6
#define SPI_MISO_GPIO_PORT       GPIOA
#define SPI_MISO_GPIO_CLK        RCC_APB2PERIPH_GPIOA

//MOSI 
#define SPI_MOSI_PIN             GPIO_Pins_7
#define SPI_MOSI_GPIO_PORT       GPIOA
#define SPI_MOSI_GPIO_CLK        RCC_APB2PERIPH_GPIOA


//CS(NSS)
//#define SPI_CS_PIN               GPIO_Pins_4
//#define SPI_CS_GPIO_PORT         GPIOA
//#define SPI_CS_GPIO_CLK          RCC_APB2PERIPH_GPIOA

//CS(NSS)
//#define SPI_FLASH_CS_LOW() {SPI_CS_GPIO_PORT->BSRE=SPI_CS_PIN;}
//CS(NSS)
//#define SPI_FLASH_CS_HIGH() {SPI_CS_GPIO_PORT->BRE=SPI_CS_PIN;}

 				  	    													  
void BSP_SPI_Init(void);			             //初始化SPI口
   
u8 SPI_SendByte(u8 TxData);                      //SPI总线读写一个字节

u8 SPI_ReadByte(void);
		 
#endif

