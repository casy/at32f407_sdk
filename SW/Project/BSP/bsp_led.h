/**
  **************************************************************************
  * File   : at32_board.h
  * Version: V1.2.9
  * Date   : 2021-01-15
  * Brief  : Header file for AT-START board
  *          1. Set of firmware functions to manage Leds, push-button and COM ports.
  *          2. initialize Delay Function and USB
  */

#ifndef __BSP_LED_H
#define __BSP_LED_H	 

#include "at32f4xx.h"

/*AT_START LED*/
typedef enum
{
  LED2 = 0,  /*Red*/
  LED3,  	/*Yellow*/
  LED4   /* Green*/
}LED_Type;    

#define LED_NUM     3

#define LED2_PIN                GPIO_Pins_13
#define LED2_PORT               GPIOD
#define LED2_RCC_CLK            RCC_APB2PERIPH_GPIOD

#define LED3_PIN                GPIO_Pins_14
#define LED3_PORT               GPIOD
#define LED3_RCC_CLK       	 	RCC_APB2PERIPH_GPIOD

#define LED4_PIN                GPIO_Pins_15
#define LED4_PORT               GPIOD
#define LED4_RCC_CLK       	 	RCC_APB2PERIPH_GPIOD


void BSP_LEDs_Init(void);
void BSP_LED_ON(LED_Type led);
void BSP_LED_OFF(LED_Type led);
void BSP_LED_Toggle(LED_Type led);

#endif

