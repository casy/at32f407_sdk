/**
  **************************************************************************
  * File   : at32_board.h
  * Version: V1.2.9
  * Date   : 2021-01-15
  * Brief  : Header file for AT-START board
  *          1. Set of firmware functions to manage Leds, push-button and COM ports.
  *          2. initialize Delay Function and USB
  */

#ifndef __BSP_UART_H
#define __BSP_UART_H	 

#include "at32f4xx.h"
#include <stdio.h>
#include <string.h>

/**************** UART printf ****************/
#define AT32_PRINT_UART                USART1
#define USARTx_IRQn                    USART1_IRQn
#define USARTx_IRQ_Handler             USART1_IRQHandler

/*Tx*/
#define AT32_PRINT_UARTTX_PIN          GPIO_Pins_9 
#define AT32_PRINT_UARTTX_GPIO         GPIOA

/*Rx*/
#define AT32_PRINT_UARTRX_PIN          GPIO_Pins_10
#define AT32_PRINT_UARTRX_GPIO         GPIOA



void BSP_USART1_Init(uint32_t baud);


#endif

