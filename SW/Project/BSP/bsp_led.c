/**
  **************************************************************************
  * File   : bsp_led.c
  * Version: V1.2.9
  * Date   : 2021-01-15
  * Brief  : 移植只需要修改头文件中的宏定义即可
  **************************************************************************
  */

#include "bsp_led.h"


#ifdef __cplusplus
namespace std
{
  extern "C" {
#endif


/*AT-START LED resouce array*/
GPIO_Type *LED_PORT[LED_NUM] = {LED2_PORT, LED3_PORT, LED4_PORT};
uint16_t LED_PIN[LED_NUM]    = {LED2_PIN, LED3_PIN, LED4_PIN};
uint32_t LED_RCC_CLK[LED_NUM] = {LED2_RCC_CLK, LED3_RCC_CLK, LED4_RCC_CLK};


void BSP_LEDs_Init(void)
{
	GPIO_InitType GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	
	/*Enable the LED Clock*/
	RCC_APB2PeriphClockCmd(LED_RCC_CLK[0], ENABLE);
	RCC_APB2PeriphClockCmd(LED_RCC_CLK[1], ENABLE);
	RCC_APB2PeriphClockCmd(LED_RCC_CLK[2], ENABLE);
	/*Configure the LED pin as ouput push-pull*/
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;
	GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;	
	
	GPIO_InitStructure.GPIO_Pins = LED_PIN[0];
	GPIO_Init(LED_PORT[0], &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pins = LED_PIN[1];
	GPIO_Init(LED_PORT[1], &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pins = LED_PIN[2];
	GPIO_Init(LED_PORT[2], &GPIO_InitStructure);
}


void BSP_LED_ON(LED_Type led)
{
	 if( led > (LED_NUM - 1))
	 {
		 return;
	 }
	 LED_PORT[led]->BRE = LED_PIN[led];
}


void BSP_LED_OFF(LED_Type led)
{
	 if( led > (LED_NUM - 1))
	 {
		 return;
	 }
	 LED_PORT[led]->BSRE = LED_PIN[led];
}


void BSP_LED_Toggle(LED_Type led)
{
	if( led > (LED_NUM - 1))
	 {
		 return;
	 }
	 LED_PORT[led]->OPTDT ^= LED_PIN[led];
}


