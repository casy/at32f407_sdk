#include "bsp_dht12.h"
#include "bsp_i2c.h"
#include "./UART/bsp_uart.h"
#include <stdio.h>

/*
说明：
2.7-5.5v
湿度精度 5%
温度精度0.5
采样周期两秒,根据手册要求
人体适宜湿度45-65%RH
*/

unsigned char Dht12_Addr[5] = {0x00, 0x01, 0x02, 0x03,0x04};    //湿度整数部分，小数部分，温度度整数部分，小数部分,校验位
unsigned char Dht12_Data[5] = {0x00, 0x00, 0x00, 0x00,0x00};    //湿度整数部分，小数部分，温度度整数部分，小数部分,校验位

void Dht12_Read(void)
{
	u8 temp;
	float Temprature,Humi;//定义温湿度变量

	I2C_BufferRead( DHT12_I2C_ADDRESS , Dht12_Addr[0], 5, Dht12_Data);
  	
	temp = (u8)(Dht12_Data[0]+Dht12_Data[1]+Dht12_Data[2]+Dht12_Data[3]);//只取低8位
	if(Dht12_Data[4]==temp)//如果校验成功，往下运行
	{
		Humi=Dht12_Data[0]*10+Dht12_Data[1]; //湿度

		if(Dht12_Data[3]&0X80)	//为负温度
		{
			Temprature =0-(Dht12_Data[2]*10+((Dht12_Data[3]&0x7F)));
		}
		else   //为正温度
		{
			Temprature=Dht12_Data[2]*10+Dht12_Data[3];//为正温度
		} 	
		//判断温湿度是否超过测量范围（温度：-20℃~60摄氏度；湿度：20%RH~95%RH）		
		if(Humi>950) 
		{
			Humi=950;
		}
		if(Humi<200)
		{
			Humi =200;
		}
		if(Temprature>600)
		{
			Temprature=600;
		}
		if(Temprature<-200)
		{
			Temprature = -200;
		}
		Temprature=Temprature/10;//计算为温度值
		Humi=Humi/10; //计算为湿度值
		printf("\r\n温度为:  %.1f  ℃\r\n",Temprature); //显示温度
		printf("湿度为:  %.1f  %%RH\r\n",Humi);//显示湿度		   
	}
	else //校验失败
	{
		printf("CRC Error !!\r\n");
	}
}
