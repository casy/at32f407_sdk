/*
 **************************************************************************
 * File Name    : usb_int.h
 * Description  : Endpoint CTRF (Low and High) interrupt's service routines prototypes
 * Date         : 2016-12-14
 * Version      : V1.0.0
 **************************************************************************
 */



/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_INT_H
#define __USB_INT_H

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void CTR_LP(void);
void CTR_HP(void);

/* External variables --------------------------------------------------------*/

#endif /* __USB_INT_H */


