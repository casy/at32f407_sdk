/*
 **************************************************************************
 * Copyright (C) 2016 by ARTERY Technology Co., Ltd. All Rights Reserved.  
 **************************************************************************
 * THIS SOURCE FILE IS DISTRIBUTED IN THE HOPE THAT CAN REDUCE EFFORTS AND
 * TIME, BUT WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **************************************************************************
 * File Name    : platform_config.h
 * Description  :
 * Date         : 2016-12-14
 * Version      : V1.0.0
 **************************************************************************
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PLATFORM_CONFIG_H
#define __PLATFORM_CONFIG_H

/* Includes ------------------------------------------------------------------*/
//#include "at32f40x_type.h" //wonder mark, for compatibility

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Uncomment the line corresponding to the Artery Technology evaluation board
   used to run the example */

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif /* __PLATFORM_CONFIG_H */



