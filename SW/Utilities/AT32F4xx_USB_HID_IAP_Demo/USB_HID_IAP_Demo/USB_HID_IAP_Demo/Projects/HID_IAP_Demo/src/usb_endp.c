/*
 **************************************************************************
 * File Name    : usb_endp.c
 * Description  : Endpoint routines
 * Date         : 2016-12-14
 * Version      : V1.0.0
 **************************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "usb_lib.h"
#include "usb_istr.h"
#include "iap_user.h"
#include "at_start_f403.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t Receive_Buffer[2];
extern __IO uint8_t PrevXferComplete;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
* Function Name  : EP1_OUT_Callback.
* Description    : EP1 OUT Callback Routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP2_OUT_Callback(void)
{
  uint32_t data_len = 0;
  /* Read received data (n bytes) */  
  data_len = USB_SIL_Read(EP2_OUT, IAP_Info.IAP_Rx_Buffer);
  if (data_len > 2 )
      IAP_Command_Parse(IAP_Info.IAP_Rx_Buffer, data_len);
  SetEPRxStatus(ENDP2, EP_RX_VALID);
 
}

/*******************************************************************************
* Function Name  : EP1_IN_Callback.
* Description    : EP1 IN Callback Routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP1_IN_Callback(void)
{
  IAP_Info.IAP_Tx_status = 0;
  if ( IAP_Info.IAP_Step == IAP_JMP_PRE )
  {
      IAP_Info.IAP_Step = IAP_JUMP;
  }
}


/*******************************************************************************
* Function Name  : EP1_Respond_Data.
* Description    : EP1 IN EP1_Respond_Data Routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP1_Respond_Data(uint8_t *res_buf, uint32_t len)
{
  USB_SIL_Write(EP1_IN, res_buf, len);
  SetEPTxStatus(ENDP1, EP_TX_VALID);
}
 

