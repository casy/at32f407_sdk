/*
 **************************************************************************
 * File Name    : main.c
 * Description  : HID demo main file
 * Date         : 2016-12-14
 * Version      : V1.0.0
 **************************************************************************/

#include "usb_lib.h"
#include "hw_config.h"
#include "usb_pwr.h"
#include "at_start_f403.h"
#include "iap_user.h"
#include "at_spi_flash.h"

int main(void)
{	
//    uint8_t istr[64];

    uint32_t LedTimer = 0, LedTog = SystemCoreClock/40;
    at_start_f403_init();
    /*Check if the key Push button*/
    if ( AT_BUTTON_State(BUTTON_WAKEUP) == 0x00)
    {
        /*Check Flag*/
        if (IAP_get_flag() == IAP_SUCCESS)
        {
            /*Jump to User Code*/
            IAP_Jump(FLASH_APP1_ADDR);
        }
    }
    Flash_spi_init();
 	USB_Interrupts_Config();    
    Set_USBClock(0);
 	USB_Init();	    
 	while(1)
	{
        IAP_handle_loop();
        if ( LedTimer == LedTog)
        {
            AT_LEDn_Toggle(LED2);
            LedTimer = 0;
        }
        LedTimer ++;
        
        
	}	   										    			    
}





















