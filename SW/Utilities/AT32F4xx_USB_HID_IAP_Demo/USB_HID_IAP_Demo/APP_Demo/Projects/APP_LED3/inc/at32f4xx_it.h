/*
 **************************************************************************
 * Copyright (C) 2016 by ARTERY Technology Co., Ltd. All Rights Reserved.
 **************************************************************************
 * THIS SOURCE FILE IS DISTRIBUTED IN THE HOPE THAT CAN REDUCE EFFORTS AND
 * TIME, BUT WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **************************************************************************
 * File Name    : at32f403_it.h
 * Description  :
 * Date         : 2017-10-24
 * Version      : V1.0.0
 **************************************************************************
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __at32f403_INT_H
#define __at32f403_INT_H

/* Includes ------------------------------------------------------------------*/
#include "at32f4xx.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

void NMI_Handler(void);
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
void SVC_Handler(void);
void DebugMon_Handler(void);
void PendSV_Handler(void);
void SysTick_Handler(void);

#endif /* __at32f403_INT_H */


