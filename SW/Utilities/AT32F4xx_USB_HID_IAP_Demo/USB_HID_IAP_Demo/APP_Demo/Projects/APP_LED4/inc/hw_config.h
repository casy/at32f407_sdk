/*
 **************************************************************************
 * File Name    : hw_config.h
 * Description  : Hardware Configuration & Setup
 * Date         : 2016-12-14
 * Version      : V1.0.0
 **************************************************************************
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HW_CONFIG_H
#define __HW_CONFIG_H

/* Includes ------------------------------------------------------------------*/
#include <at32f4xx.h>
#include "platform_config.h"
#include "usb_type.h"


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
#define DOWN            1
#define LEFT            2
#define RIGHT           3
#define UP              4
#define CURSOR_STEP     40

/* Exported functions ------------------------------------------------------- */
void Set_System(void);
void Set_USBClock(unsigned char bDivByPRE);
void GPIO_AINConfig(void);
void Enter_LowPowerMode(void);
void Leave_LowPowerMode(void);
void USB_Interrupts_Config(void);
void USB_Cable_Config (FunctionalState NewState);

void Get_SerialNum(void);
void AT32_BTNClick(unsigned int iIdx, unsigned int bPush);
#endif  /*__HW_CONFIG_H*/


