/*
 **************************************************************************
 * File Name    : main.c
 * Description  : HID demo main file
 * Date         : 2016-12-14
 * Version      : V1.0.0
 **************************************************************************/
#include "at_start_f403.h"
#define APPLICATION_ADDRESS 0x08004000
int main(void)
{	
    uint32_t LedTimer = 0, LedTog = SystemCoreClock/40;
    SCB->VTOR = APPLICATION_ADDRESS; 
    at_start_f403_init();         
 	while(1)
	{
        if ( LedTimer == LedTog)
        {
            AT_LEDn_Toggle(LED4);
            LedTimer = 0;
        }
        LedTimer ++;
        
        
	}	   										    			    
}





















