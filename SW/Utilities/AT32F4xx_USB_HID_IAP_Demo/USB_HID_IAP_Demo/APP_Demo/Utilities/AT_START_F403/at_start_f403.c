/*
 **************************************************************************
 * File Name    : at_start_f403.c
 * Description  : 1. Set of firmware functions to manage Leds, push-button and COM ports.
 *                2. initialize Delay Function and USB
 * Date         : 2016-12-14
 * Version      : V1.0.0
 **************************************************************************
 */

#include "at_start_f403.h"
#include "stdio.h"


#ifdef AT_START_F403
GPIO_Type *LED_GPIO_PORT[LED_NUM] = {LED1_GPIO, LED2_GPIO, LED3_GPIO, LED4_GPIO};
uint16_t LED_GPIO_PIN[LED_NUM]    = {LED1_PIN, LED2_PIN, LED3_PIN, LED4_PIN};
uint32_t LED_GPIO_RCC_CLK[LED_NUM] = {LED1_GPIO_RCC_CLK, LED2_GPIO_RCC_CLK, LED3_GPIO_RCC_CLK, LED4_GPIO_RCC_CLK};
#else
GPIO_Type *LED_GPIO_PORT[LED_NUM] = {LED1_GPIO, LED2_GPIO};
uint16_t LED_GPIO_PIN[LED_NUM]    = {LED1_PIN, LED2_PIN};
uint32_t LED_GPIO_RCC_CLK[LED_NUM] = {LED1_GPIO_RCC_CLK, LED2_GPIO_RCC_CLK};
#endif

#ifdef AT_START_F403
GPIO_Type *BUTTON_GPIO_PORT[BUTTON_NUM] = {BUTTON_WAKEUP_GPIO};
uint16_t BUTTON_GPIO_PIN[BUTTON_NUM]    = {BUTTON_WAKEUP_PIN};
uint32_t BUTTON_GPIO_RCC_CLK [BUTTON_NUM] = {BUTTON_WAKEUP_RCC_CLK};
#else
GPIO_Type *BUTTON_GPIO_PORT[BUTTON_NUM] = {BUTTON_WAKEUP_GPIO, BUTTON_KEY0_RIGHT_GPIO, BUTTON_KEY0_DOWN_GPIO, BUTTON_KEY0_LEFT_GPIO};
uint16_t BUTTON_GPIO_PIN[BUTTON_NUM]    = {BUTTON_WAKEUP_PIN, BUTTON_KEY0_RIGHT_PIN, BUTTON_KEY0_DOWN_PIN, BUTTON_KEY0_LEFT_PIN};
uint32_t BUTTON_GPIO_RCC_CLK [BUTTON_NUM] = {BUTTON_WAKEUP_RCC_CLK, BUTTON_KEY0_RIGHT_RCC_CLK, BUTTON_KEY0_LEFT_RCC_CLK};
#endif

static __IO uint32_t Time_Delay;

struct __FILE 
{ 
	int handle; 
}; 

FILE __stdout;       
 
_sys_exit(int x) 
{ 
	x = x; 
} 
int fputc(int ch, FILE *f)
{      
	while((USART1->STS&0X40)==0); 
    USART1->DT = (u8) ch;      
	return ch;
}

/**
  * @brief  Board initialize interface        
  * @param  None
  * @retval None
  */
void at_start_f403_init()
{
    delay_init();
#ifdef AT_START_F403
    /*Configure LED in AT_START_F403*/
    AT_LEDn_Init(LED2);
    AT_LEDn_Init(LED3);
    AT_LEDn_Init(LED4);
    AT_LEDn_OFF(LED2);
    AT_LEDn_OFF(LED3);
    AT_LEDn_OFF(LED4);
    
    /*Configure Button in AT_START_F403*/
    AT_BUTTON_Init(BUTTON_WAKEUP);
#else
    /*Configure LED in Demo board*/
    AT_LEDn_Init(LED1);
    AT_LEDn_Init(LED2);
    AT_LEDn_OFF(LED1);
    AT_LEDn_OFF(LED2);
    
    /*Configure Boutton in Demo board*/
    AT_BUTTON_Init(BUTTON_WAKEUP);
    AT_BUTTON_Init(BUTTON_KEY0_RIGHT);
    AT_BUTTON_Init(BUTTON_KEY1_DOWN);
    AT_BUTTON_Init(BUTTON_KEY2_LEFT);
#endif    

    /*Configure USB */
//    AT_USB_GPIO_init();
    
}

/**
  * @brief  USB GPIO initialize
  *         USB use DP->PA12, DM->PA11    
  * @param  None
  * @retval None
  */
void AT_USB_GPIO_init()
{
    GPIO_InitType GPIO_InitStructure;
    /* Enable the USB Clock*/
    RCC_APB2PeriphClockCmd(USB_GPIO_RCC_CLK, ENABLE);
    
    /*Configure DP, DM pin as GPIO_Mode_OUT_PP*/
    GPIO_InitStructure.GPIO_Pins  = USB_DP_PIN | USB_DM_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;
    GPIO_Init(USB_GPIO, &GPIO_InitStructure);
    GPIO_ResetBits(USB_GPIO, USB_DP_PIN);
    
}


/**
  * @brief  Configure Button GPIO   
  * @param  Button: Specifies the Button to be configured.
  * @retval None
  */
void AT_BUTTON_Init(BUTTON_Type button)
{
    GPIO_InitType GPIO_InitStructure;
    
    /*Enable the Button Clock*/
    RCC_APB2PeriphClockCmd(BUTTON_GPIO_RCC_CLK[button], ENABLE);
    
    /*Configure Button pin as input with pull-up/pull-down*/
    GPIO_InitStructure.GPIO_Pins = BUTTON_GPIO_PIN[button];
    if ( BUTTON_WAKEUP == button )
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_PD;  
    else
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_PU; 
    GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;	
    GPIO_Init(BUTTON_GPIO_PORT[button], &GPIO_InitStructure);
}

/**
  * @brief  Returns the selected button state   
  * @param  Button: Specifies the Button to be Checked
  * @retval The Button GPIO Pin value
  */
uint32_t AT_BUTTON_State(BUTTON_Type button)
{
    return GPIO_ReadInputDataBit(BUTTON_GPIO_PORT[button], BUTTON_GPIO_PIN[button]);
}


/**
  * @brief  Returns which Button have press down 
  * @param  None
  * @retval The Button have press down
  */
BUTTON_Type AT_BUTTON_Press()
{
#ifdef AT_START_F403
    /*Get Wakeup Pin state in AT_START_F403 board*/
    if ( (AT_BUTTON_State(BUTTON_WAKEUP) == Bit_SET ) )
    {
        /*debounce*/
        delay_ms(30);
        if (AT_BUTTON_State(BUTTON_WAKEUP) == Bit_SET)
            return BUTTON_WAKEUP;
    }
#else
    if ( (AT_BUTTON_State(BUTTON_WAKEUP) == Bit_SET ) || (AT_BUTTON_State(BUTTON_KEY0_RIGHT) == Bit_RESET ) ||
        (AT_BUTTON_State(BUTTON_KEY1_DOWN) == Bit_RESET ) || (AT_BUTTON_State(BUTTON_KEY2_LEFT) == Bit_RESET ) )
    {
        /*debounce*/
        delay_ms(30);
        
        if (AT_BUTTON_State(BUTTON_WAKEUP) == Bit_SET)
            return BUTTON_WAKEUP;
        if (AT_BUTTON_State(BUTTON_KEY0_RIGHT) == Bit_RESET )
            return BUTTON_KEY0_RIGHT;
        if (AT_BUTTON_State(BUTTON_KEY1_DOWN) == Bit_RESET )
            return BUTTON_KEY1_DOWN;
        if (AT_BUTTON_State(BUTTON_KEY2_LEFT) == Bit_RESET)
            return BUTTON_KEY2_LEFT;
    }
 #endif
    return NO_BUTTON;
}

/**
  * @brief  Configure LED GPIO   
  * @param  LED: Specifies the LED to be configured.
  * @retval None
  */
void AT_LEDn_Init(LED_Type led)
{
    GPIO_InitType GPIO_InitStructure;
    /*Enable the LED Clock*/
    RCC_APB2PeriphClockCmd(LED_GPIO_RCC_CLK[led], ENABLE);
    
    /*Configure the LED pin as ouput push-pull*/
    GPIO_InitStructure.GPIO_Pins = LED_GPIO_PIN[led];				 
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP; 		 
    GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;	
    GPIO_Init(LED_GPIO_PORT[led], &GPIO_InitStructure);
    
}

/**
  * @brief  Turns selected LED On.
  * @param  Led: Specifies the Led to be set on. 
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4  
  * @retval None
  */
void AT_LEDn_ON(LED_Type led)
{
    if ( led > (LED_NUM - 1))
        return;
    if ( LED_GPIO_PIN[led] )
        LED_GPIO_PORT[led]->BRE = LED_GPIO_PIN[led];
}

/**
  * @brief  Turns selected LED Off.
  * @param  Led: Specifies the Led to be set off. 
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4 
  * @retval None
  */
void AT_LEDn_OFF(LED_Type led)
{
    if ( led > (LED_NUM - 1))
        return;
    if ( LED_GPIO_PIN[led] )
        LED_GPIO_PORT[led]->BSRE = LED_GPIO_PIN[led];
}

/**
  * @brief  Turns selected LED Tooggle.
  * @param  Led: Specifies the Led to be set off. 
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4 
  * @retval None
  */
void AT_LEDn_Toggle(LED_Type led)
{
    if ( led > (LED_NUM - 1))
        return;
    if ( LED_GPIO_PIN[led] )
        LED_GPIO_PORT[led]->OPTDT ^= LED_GPIO_PIN[led];
}


/**
  * @brief  initialize UART1   
  * @param  bound: UART BaudRate
  * @retval None
  */
void UART_Init(uint32_t bound)
{
    GPIO_InitType GPIO_InitStructure;
	USART_InitType USART_InitStructure;
	NVIC_InitType NVIC_InitStructure;
	
    /*Enable the UART1 Clock*/
    RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_USART1|RCC_APB2PERIPH_GPIOA, ENABLE);	
  
	/* Configure the UART1 RX pin */
    GPIO_InitStructure.GPIO_Pins = GPIO_Pins_9; 
    GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	
    GPIO_Init(GPIOA, &GPIO_InitStructure);
   
    /* Configure the UART1 TX pin */
    GPIO_InitStructure.GPIO_Pins = GPIO_Pins_10;//PA10
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    /*Enable and Set UART1 interrut*/
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3 ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;	
	NVIC_Init(&NVIC_InitStructure);	
    
    /*Configure UART1 param*/
	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	

    USART_Init(USART1, &USART_InitStructure); 
    USART_INTConfig(USART1, USART_INT_RDNE, ENABLE);
    USART_Cmd(USART1, ENABLE);   
}

/**
  * @brief  initialize Delay function   
  * @param  None
  * @retval None
  */		   
void delay_init()
{
    /*Config Systick*/
    if ( SysTick_Config(SystemCoreClock / 1000) )
    {
        while ( 1 );
    }
}

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement()
{
    if ( Time_Delay != 0x00)
        Time_Delay --;
}

		    								  
/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in milliseconds.
  * @retval None
  */
void delay_ms(u16 nms)
{
    Time_Delay = nms;
    while (Time_Delay != 0x00);     	    
} 
















