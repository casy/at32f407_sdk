/*
 **************************************************************************
 * File Name    : at_start_f403.h
 * Description  : 1. Set of firmware functions to manage Leds, push-button and COM ports.
 *                2. initialize Delay Function and USB
 * Date         : 2016-12-14
 * Version      : V1.0.0
 **************************************************************************
 */

#ifndef __AT32F403_MAIN_BOARD_H
#define __AT32F403_MAIN_BOARD_H	 
#include <at32f4xx.h>

/*define usb pin*/
#define USB_DP_PIN          GPIO_Pins_12
#define USB_DM_PIN          GPIO_Pins_11

#define USB_GPIO            GPIOA
#define USB_GPIO_RCC_CLK    RCC_APB2PERIPH_GPIOA

/*AT_START_F403 LED*/
typedef enum
{
    LED1 = 0,
    LED2,
    LED3,
    LED4
}LED_Type;    

#ifdef AT_START_F403
#define LED_NUM     4

/*have no LED1*/
#define LED1_PIN                0
#define LED1_GPIO               NULL
#define LED1_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOB

#define LED2_PIN                GPIO_Pins_13
#define LED2_GPIO               GPIOD
#define LED2_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOD

#define LED3_PIN                GPIO_Pins_14
#define LED3_GPIO               GPIOD
#define LED3_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOD

#define LED4_PIN                GPIO_Pins_15
#define LED4_GPIO               GPIOD
#define LED4_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOD

#else

#define LED_NUM     2

#define LED1_PIN                GPIO_Pins_5
#define LED1_GPIO               GPIOB
#define LED1_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOB

#define LED2_PIN                GPIO_Pins_5
#define LED2_GPIO               GPIOE
#define LED2_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOE

#define LED3_PIN                
#define LED3_GPIO
#define LED3_GPIO_RCC_CLK

#define LED4_PIN
#define LED4_GPIO
#define LED4_GPIO_RCC_CLK
#endif

/*End LED define*/


/*define button*/
typedef enum
{
    BUTTON_WAKEUP = 0,
    BUTTON_KEY0_RIGHT,
    BUTTON_KEY1_DOWN,
    BUTTON_KEY2_LEFT,   
    NO_BUTTON    
}BUTTON_Type;

#ifdef AT_START_F403

#define BUTTON_NUM  1

#define BUTTON_WAKEUP_PIN              GPIO_Pins_0
#define BUTTON_WAKEUP_GPIO             GPIOA
#define BUTTON_WAKEUP_RCC_CLK          RCC_APB2PERIPH_GPIOA

#else
#define BUTTON_NUM  4

#define BUTTON_WAKEUP_PIN              GPIO_Pins_0
#define BUTTON_WAKEUP_GPIO             GPIOA
#define BUTTON_WAKEUP_RCC_CLK          RCC_APB2PERIPH_GPIOA

#define BUTTON_KEY0_RIGHT_PIN          GPIO_Pins_4
#define BUTTON_KEY0_RIGHT_GPIO         GPIOE
#define BUTTON_KEY0_RIGHT_RCC_CLK      RCC_APB2PERIPH_GPIOE

#define BUTTON_KEY0_DOWN_PIN           GPIO_Pins_3
#define BUTTON_KEY0_DOWN_GPIO          GPIOE
#define BUTTON_KEY0_DOWN_RCC_CLK       RCC_APB2PERIPH_GPIOE

#define BUTTON_KEY0_LEFT_PIN           GPIO_Pins_2
#define BUTTON_KEY0_LEFT_GPIO          GPIOE
#define BUTTON_KEY0_LEFT_RCC_CLK       RCC_APB2PERIPH_GPIOE
#endif
/*end define button*/

/*Audio DAC OUTPUT GPIO Pin*/
#define F403_AUDIO_DAC_OUTPIN      GPIO_Pins_4

void at_start_f403_init(void);
void AT_USB_GPIO_init(void);

void AT_LEDn_Init(LED_Type led);
void AT_LEDn_ON(LED_Type led);
void AT_LEDn_OFF(LED_Type led);
void AT_LEDn_Toggle(LED_Type led);

void AT_BUTTON_Init(BUTTON_Type button);
uint32_t AT_BUTTON_State(BUTTON_Type button);
BUTTON_Type AT_BUTTON_Press(void);

//uint8_t at32f403_key_init(void);
//uint8_t at32f403_key_press(void);
void UART_Init(uint32_t bound);

void delay_init(void);
void delay_ms(u16 nms);
void delay_us(u32 nus);
void TimingDelay_Decrement(void);
#endif
